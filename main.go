package main

import (
	"bufio"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"io/fs"
	"os"
)

type (
	Model struct {
		Files   []ModelFile
		Folders []string
	}
	ModelFile struct {
		Path string
		Hash string
	}
)

func main() {
	var m Model
	f := os.DirFS(".")
	hasher := sha256.New()
	fs.WalkDir(f, ".", func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() {
			m.Folders = append(m.Folders, path)
		} else {
			f, err := os.OpenFile(path, os.O_RDONLY, fs.FileMode(os.O_RDONLY))
			if err != nil {
				return err
			}
			buf := bufio.NewReader(f)
			defer f.Close()
			_, err = io.Copy(hasher, buf)
			if err != nil {
				return err
			}
			h := hex.EncodeToString(hasher.Sum(nil))
			m.Files = append(m.Files, ModelFile{Path: path, Hash: h})
			defer hasher.Reset()
		}
		return nil
	})
	// fmt.Println(m)
	j, err := json.Marshal(m)
	if err != nil {
		panic(err)
	}
	fmt.Println(string(j))
}
